//  Author:Fred.Chu
//  Date:9 April,2013
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation; either
//  version 2.1 of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
//  Modified record:
//  07.01.2018 AlexGyver
//  Added a lot of functions for display custom symbols and int values
//
//  Подробное описание здесь http://alexgyver.ru/tm1637_display/
//  Текущая версия: 1.1 от 29.03.2018
//
//  Modified record:
//  03.09.2018 AlexanderMishin13
//  Adapted for libgpio library (FreeBSD native)
//  26/09/2018 AlexanderMishin13
//  Added display("12:34") function 
//
/*******************************************************************************/

#ifndef TM1637_h
#define TM1637_h
#include <sys/types.h>
#include <libgpio.h>
#include <unistd.h>
#include <stdbool.h>
#include <stdio.h>

//************definitions for TM1637*********************

#define ACK_TIMEOUT   200
#define MAX_COLOM       4

#define ADDR_AUTO    0x40
#define ADDR_FIXED   0x44

#define STARTADDR    0xc0
#define DISPLAY_OFF  0x80
#define DISPLAY_CTRL 0x88
/**** definitions for the clock point of the digit tube *******/
#define POINT_ON     true
#define POINT_OFF   false
/**************definitions for brightness***********************/
#define  BRIGHT_DARKEST 0
#define  BRIGHT_TYPICAL 2
#define  BRIGHTEST      7
/************** БУКВЫ И СИМВОЛЫ *****************/
#define _A 0x77
#define _B 0x7f
#define _C 0x39
#define _D 0x3f
#define _E 0x79
#define _F 0x71
#define _G 0x3d
#define _H 0x76
#define _J 0x1e
#define _L 0x38
#define _N 0x37
#define _O 0x3f
#define _P 0x73
#define _S 0x6d
#define _U 0x3e
#define _Y 0x6e
#define _a 0x5f
#define _b 0x7c
#define _c 0x58
#define _d 0x5e
#define _e 0x7b
#define _f 0x71
#define _h 0x74
#define _i 0x10
#define _j 0x0e
#define _l 0x06
#define _n 0x54
#define _o 0x5c
#define _q 0x67
#define _r 0x50
#define _t 0x78
#define _u 0x1c
#define _y 0x6e
#define _dash 0x40
#define _under 0x08
#define _equal 0x48
#define _empty 0x00

#define _0 0x3f
#define _1 0x06
#define _2 0x5b
#define _3 0x4f
#define _4 0x66
#define _5 0x6d
#define _6 0x7d
#define _7 0x07
#define _8 0x7f
#define _9 0x6f
/************** БУКВЫ И СИМВОЛЫ *****************/


class TM1637 {
  public:
    uint8_t Cmd_SetData;
    uint8_t Cmd_SetAddr;
    uint8_t Cmd_DispCtrl;
    mutable bool _PointFlag;     //_PointFlag=true: the clock point on
    TM1637(gpio_pin_t pinClk, gpio_pin_t pinData);
    void init(void) const;        //To clear the display
    void writeByte(int8_t wr_data) const;//write 8bit data to tm1637
    void start(void) const;//send start bits
    void stop(void) const; //send stop bits

    void display(char*) const;                                // вывести строку ЦИФР
    void display(int8_t DispData[]) const;                    // вывести массив ЦИФР
    void displayByte(int8_t DispData[]) const;                // вывести массив БАЙТОВ
    void display(uint8_t BitAddr,int8_t DispData) const;      // вывести цифру в указанный порт
    void displayByte(uint8_t BitAddr,int8_t DispData) const;  // вывести байт в указанный порт
    void displayByte(int8_t bit1, int8_t bit2, int8_t bit3, int8_t bit4) const; // вывести побайтово символы
    void displayInt(int n) const;       // вывод числа (-999-9999) без нулей слева
    void displayIntZero(int n) const;   // вывод числа (-999-9999) с нулями слева
    void runningString(int8_t DispData[], uint8_t amount, unsigned int delayMs) const;

    void clearDisplay(void) const;
    void displayOff(void) const;
    void displayOn(void) const;
    bool isOff(void) const;
    bool isOn(void) const;
    /* Set the display brightness on the next data output. */
    void set(uint8_t = BRIGHT_TYPICAL, uint8_t = ADDR_AUTO, uint8_t = STARTADDR);
    /* Whether to light the clock point ":" on the next data output. */
    void point(bool PointFlag) const;

    void coding(int8_t DispData[]) const;
    void codingByte(int8_t DispData[]) const;
    int8_t coding(int8_t DispData) const;
    int8_t codingByte(int8_t DispData) const;
  private:
    mutable bool on;
    mutable uint8_t brightness;
    gpio_handle_t gpio_handle;
    gpio_pin_t Clkpin;
    gpio_pin_t Datapin;
};
#endif

CFLAGS=-fpic -c -Wall -O2
PREFIX=/usr/local
MAJOR=1

all: libtm1637.so

tm1637.o: tm1637.cpp tm1637.h
	$(CXX) $(CFLAGS) tm1637.cpp

libtm1637.so: tm1637.o
	$(CXX) --shared -fpic -Wl,-soname,libtm1637.so -o libtm1637.so.${MAJOR} tm1637.o -lc

clean:
	rm *.o libtm1637.so* demo demo2

install: libtm1637.so
	sudo install -m 0644 -o root -g wheel tm1637.h ${PREFIX}/include/
	sudo install -m 0755 -o root -g wheel -s libtm1637.* ${PREFIX}/lib/
	sudo install -lr ${PREFIX}/lib/libtm1637.so.${MAJOR} ${PREFIX}/lib/libtm1637.so

uninstall:
	sudo rm ${PREFIX}/include/tm1637.h
	sudo rm ${PREFIX}/lib/libtm1637.so*

demo.o: demo.cpp
	$(CXX) -c -Wall -O2 -I/usr/local/include demo.cpp

demo: demo.o
	$(CXX) demo.o -ltm1637 -lgpio -lc -L/usr/local/lib -o demo

demo2.o: demo2.cpp
	$(CXX) -c -Wall -O2 -I/usr/local/include demo2.cpp

demo2: demo2.o
	$(CXX) demo2.o -ltm1637 -lgpio -lc -L/usr/local/lib -o demo2


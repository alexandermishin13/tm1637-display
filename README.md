# tm1637-display

![TM1637](/tm1637.jpg?raw=true "TM1637")

## About

A FreeBSD version of a **tm1637-display** library written by *Fred.Chu* for Arduino.

Added a lot of functions for display custom symbols and int values by *AlexGyver*.
Adapted (by *me*) for FreeBSD native library libgpio.

But I personally prefer to use my other try to control the tm1637 display:

[tm1637-kmod](https://gitlab.com/alexandermishin13/tm1637-kmod)

## Hardware and software

I tested the library on my **Raspberry Pi 2** with **FreeBSD 12** installed on it.
I'm sure that everything will be fine with **Raspberry Pi 3** and|or **FreeBSD 11**.
And I think that with other **ARM SoCs** will also be fine.

## Dependencies

For a library installation process You need:
* a c++ compatible compilator, e.g., `clang++` (already in Your system);
* a `libgpio.so` library (In Your system starting from FreeBSD 11.0)

Also You need to link Your program with `-ltm1637 -lgpio` flags

## Download

```
git clone https://gitlab.com/alexandermishin13/tm1637-display.git
```

## Compilation

To install the library use:
```
make
sudo make install
```
To make demo programs use:
```
make demo
./demo
make demo2
./demo2
```

## Use

For start to using installed library in Your program You need to add to Your .cpp-file:
```
#include <tm1637.h>;

#define CLK 20; // Pin 38 on Raspberri Pi 2 or 3
#define DIO 21; // Pin 40

TM1637 disp(CLK, DIO);
disp.init();
disp.set(BRIGHT_TYPICAL);
disp.displayInt(1234);
```

## Demo

The `demo` program shows a system time to the **tm1637** display.

It uses **nanosleep(const timespec*, timespec*)** for a clock points blinking two times per second.
These are not exactly accurate seconds, but quite acceptable for the `demo` program.

It intercepts **SIGINT** and **SIGTERM** signals to turn the display off before the `demo` stops.
For stop the program type `Ctrl+C`.

The `demo2` program shows a running string over a display and count[-down] examples.

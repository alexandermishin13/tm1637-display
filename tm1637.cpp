//  Author:Fred.Chu
//  Date:9 April,2013
//
//  This library is free software; you can redistribute it and/or
//  modify it under the terms of the GNU Lesser General Public
//  License as published by the Free Software Foundation; either
//  version 2.1 of the License, or (at your option) any later version.
//
//  This library is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
//  Lesser General Public License for more details.
//
//  You should have received a copy of the GNU Lesser General Public
//  License along with this library; if not, write to the Free Software
//  Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
//
//  Modified record:
//
//  Modified record:
//  03.09.2018 AlexanderMishin13
//  Adapted for libgpio library (FreeBSD native)
//  13.09.2018 rewrited displayInt() and displayIntZero()
//  26/09/2018 AlexanderMishin13
//  Added display("12:34") function 
//
/*******************************************************************************/
#include "tm1637.h"
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <time.h>

/* A colom segments definition for 0..9 */
static int8_t
TubeTab[] = { _0, _1, _2, _3, _4, _5, _6, _7, _8, _9 };

/*
 * Constructor for TM1637
 */
TM1637::TM1637(gpio_pin_t pinClk, gpio_pin_t pinData) :
  Cmd_DispCtrl(DISPLAY_CTRL),
  _PointFlag(true), brightness(BRIGHT_DARKEST),
  gpio_handle(gpio_open(0)),
  Clkpin(pinClk), Datapin(pinData)
{
//  Clkpin = pinClk;
//  Datapin = pinData;
//  _PointFlag = true;
//  gpio_handle = gpio_open(0);

  if (gpio_handle == GPIO_INVALID_HANDLE) {
    printf("gpio_open failed\n");
    exit(EXIT_FAILURE);
  }

  char tm1637_clk_name[] = "tm1637_clk";
  gpio_pin_output(gpio_handle, Clkpin);
  gpio_pin_set_name(gpio_handle, Clkpin, tm1637_clk_name);
  char tm1637_dio_name[] = "tm1637_dio";
  gpio_pin_output(gpio_handle, Datapin);
  gpio_pin_set_name(gpio_handle, Datapin, tm1637_dio_name);

  set(this->brightness);
}

void
TM1637::init(void)
const
{
  this->clearDisplay();
}

void
TM1637::writeByte(int8_t wr_data)
const
{
  int i;
  int k = 0;

  /* Sent 8bit data */
  for(i=0; i<=7; i++)
  {
    /* Set the data bit, CLK is low after start */
    gpio_pin_set(gpio_handle, Datapin, (gpio_value_t)(wr_data&(0x01<<i)));//LSB first
    /* The data bit is ready */
    gpio_pin_high(gpio_handle, Clkpin);
    gpio_pin_low(gpio_handle, Clkpin);
  }
  /* Wait for the ACK */
  gpio_pin_input(gpio_handle, Datapin);
  gpio_pin_high(gpio_handle, Clkpin);

  do {
    if (k++<ACK_TIMEOUT)
      break;
    usleep(1);
  } while (gpio_pin_get(gpio_handle, Datapin));

  gpio_pin_low(gpio_handle, Clkpin);
  gpio_pin_output(gpio_handle, Datapin);
}

/*
 * Sends a signal to tm1637 to start a transmition a byte
 */
void
TM1637::start(void)
const
{
  gpio_pin_high(gpio_handle, Clkpin);
  gpio_pin_high(gpio_handle, Datapin); 
  gpio_pin_low(gpio_handle, Datapin); 
  gpio_pin_low(gpio_handle, Clkpin); 
} 

/*
 * Sends a signal to tm1637 to stop a transmition a byte
 */
void
TM1637::stop(void)
const
{
  gpio_pin_low(gpio_handle, Datapin);
  gpio_pin_low(gpio_handle, Clkpin);
  gpio_pin_high(gpio_handle, Clkpin);
  gpio_pin_high(gpio_handle, Datapin); 
}

/*
 * Displays a string like '12:34' or '12 34'
 */
void
TM1637::display(char *pString)
const
{
  int8_t SegData;

  start(); // Start a byte
  writeByte(ADDR_AUTO);
  stop();  // Stop a byte

  start(); // Start a byte sequence
  writeByte(Cmd_SetAddr);
  for(uint8_t i = 0; i <= MAX_COLOM; i++)
  {
    /* If digit get segments value else segments value is zero */
    if (pString[i] >= '0' && pString[i] <= '9' )
    {
      SegData = TubeTab[pString[i] & 0xf];

    }
    else
      SegData = _empty;

    /* If 2nd digit and 3rd character is colon.
       Skip just checked 3rd character for next loop
     */
    if (i == 1)
      if (pString[++i] == ':')
        SegData |= 0x80;

    writeByte(SegData);
  }
  stop(); // Stop a byte sequence
}

//display function.Write to full-screen.
void
TM1637::display(int8_t DispData[])
const
{
  int8_t SegData;

  start(); // Start a byte
  writeByte(ADDR_AUTO); // Send an address autoincrement command to tm1637
  stop();  // Stop a byte

  start(); // Start a byte sequence
  writeByte(Cmd_SetAddr); // Send a start address to tm1637
  for(uint8_t i = 0; i < MAX_COLOM; i++)
  {
    /* 0x7f is a blank symbol */
    if (DispData[i] == 0x7f)
      SegData = _empty;
    else
      SegData = TubeTab[DispData[i]];

    /* If 2nd digit and 3rd character is colon.
       Skip just checked 3rd character for next loop
     */
    if (i == 1)
      if (_PointFlag)
        SegData |= 0x80;

    writeByte(SegData); // Send colom segments to tm1637
  }
  stop(); // Stop a byte sequence
}

/*
 * Displays an array of coloms segments
 */
void
TM1637::displayByte(int8_t DispData[])
const
{
  start(); // Start a byte
  writeByte(ADDR_AUTO); // Send an address autoincrement command to tm1637
  stop();  // Stop a byte

  start(); // Start a byte sequence
  writeByte(Cmd_SetAddr); // Send a start address to tm1637
  for(uint8_t i = 0; i < MAX_COLOM; i++)
  {
    int8_t SegData = DispData[i];

    /* If 2nd digit and 3rd character is colon.
       Skip just checked 3rd character for next loop
     */
    if (i == 1)
      if (_PointFlag)
        SegData |= 0x80;

    writeByte(SegData); // Send colom segments to tm1637
  }
  stop(); // Stop a byte sequence
}

/*
 * Displays four coloms segments
 */
void
TM1637::displayByte(int8_t bit1,int8_t bit2,int8_t bit3,int8_t bit4)
const
{
  int8_t SegData[4] = {bit1, bit2, bit3, bit4};

  /* Simply calls the displayByte(int8_t DispData[]) */
  this->displayByte(SegData);
}

void
TM1637::displayIntZero(int n)
const
{
  int8_t TubeData[MAX_COLOM];

  if (n == 0)
    TubeData[MAX_COLOM - 1] = coding((int8_t) n);
  else
  {
    uint8_t sign = 0;

    if (n < 0) {
      n = -n;
      sign = 1;
      TubeData[0] = _dash;
    }

    for (uint8_t i = MAX_COLOM; i > sign; n /= 10)
      TubeData[--i] = coding((int8_t) (n % 10));
  }

  displayByte(TubeData);
}

void
TM1637::displayInt(int n)
const
{
  int8_t TubeData[MAX_COLOM] = { _empty };

  if (n == 0)
    TubeData[MAX_COLOM - 1] = coding((int8_t) n);
  else
  {
    uint8_t sign = 0;

    if (n < 0) {
      n = -n;
      sign = 1;
      TubeData[0] = _dash;
    }

    for (uint8_t i = MAX_COLOM; (n > 0) && (i > sign); n /= 10)
      TubeData[--i] = coding((int8_t) (n % 10));
  }

  displayByte(TubeData);
}

//******************************************
void
TM1637::display(uint8_t BitAddr, int8_t DispData)
const
{
  int8_t SegData;

  SegData = coding(DispData);

  start();          //start signal sent to TM1637 from MCU
  writeByte(ADDR_FIXED);//
  stop();           //

  start();          //
  writeByte(BitAddr|STARTADDR);//
  writeByte(SegData);//
  stop();            //
}

void
TM1637::displayByte(uint8_t BitAddr, int8_t DispData)
const
{
  /* Turn a colon on for second digit if needed */
  if ((BitAddr == 1) && _PointFlag)
    DispData |= 0x80;

  start(); //start signal sent to TM1637 from MCU
  writeByte(ADDR_FIXED);//
  stop();

  start();
  writeByte(BitAddr|STARTADDR);
  writeByte(DispData);
  stop();
}

void
TM1637::clearDisplay(void)
const
{
  int8_t TubeData[MAX_COLOM] = { _empty };

  displayByte(TubeData);
}

// Off the display */
void
TM1637::displayOff(void)
const
{
  start();
  writeByte(DISPLAY_OFF);
  stop();

  this->on = false; // display is off
}

/* On the diplay with current brightness */
void
TM1637::displayOn(void)
const
{
  start();
  writeByte(this->Cmd_DispCtrl|this->brightness);
  stop();

  this->on = true; // display is off
}

/* Check display is on */
bool
TM1637::isOn(void)
const
{
  return this->on;
}

/* Check display is off */
bool
TM1637::isOff(void)
const
{
  return !this->on;
}

/* Set the display brightness on the next data output. */
void
TM1637::set(uint8_t Brightness, uint8_t SetData, uint8_t SetAddr)
{
  this->brightness = Brightness;

  Cmd_SetData = SetData;
  Cmd_SetAddr = SetAddr;

  this->displayOn();
}

/* Whether to light the clock point ":" on the next data output. */
void
TM1637::point(bool PointFlag)
const
{
  _PointFlag = PointFlag;
}

void
TM1637::coding(int8_t DispData[])
const
{
  for(uint8_t i = 0; i < MAX_COLOM; i++)
    if (DispData[i] == 0x7f)
      DispData[i] = _empty;
    else
      DispData[i] = TubeTab[DispData[i]];

  /* A time colon is a second coloms segment */
  if (_PointFlag)
    DispData[1] |= 0x80;
}

void
TM1637::codingByte(int8_t DispData[])
const
{
  /* A time colon is a second coloms segment */
  if (_PointFlag)
    DispData[1] |= 0x80;
}

int8_t
TM1637::coding(int8_t DispData)
const
{
  if (DispData == 0x7f)
    DispData = _empty; //The bit digital tube off
  else
    DispData = TubeTab[DispData];

  if (_PointFlag)
    DispData |= 0x80;

  return DispData;
}

int8_t
TM1637::codingByte(int8_t DispData)
const
{
  if (_PointFlag)
    DispData |= 0x80;

  return DispData;
}

/*
 * Coverts the milliseconds to timespec structure
 */
void
convert_Ms2TS(unsigned int milliseconds, timespec *ts)
{
    ts->tv_sec  = (time_t)(milliseconds / 1000);
    ts->tv_nsec = milliseconds % 1000 * 1000000L;
}

void
TM1637::runningString(int8_t DispData[], uint8_t amount, unsigned int delayMs)
const
{
  uint8_t segData[amount + 8];    // оставляем место для 4х пустых слотов в начале и в конце

  struct timespec rqtp;
  convert_Ms2TS(delayMs, &rqtp);

  for (uint8_t i = 0; i < 4; i++)  // делаем первые 4 символа пустыми
    segData[i] = _empty;

  for (uint8_t i = 0; i < amount; i++)  // далее забиваем тем что на входе (сам текст строки)
    segData[i + 4] = DispData[i];

  for (uint8_t i = amount + 4; i < amount + 8; i++)  // и последние 4 тоже забиваем пустыми символами
    segData[i] = _empty;

  for (uint8_t i = 0; i <= amount + 4; i++) {   // выводим
    displayByte(segData[i], segData[i + 1], segData[i + 2], segData[i + 3]);
    nanosleep(&rqtp, &rqtp);
  }
}

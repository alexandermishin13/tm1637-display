#include <unistd.h>
#include <tm1637.h>
#include <time.h>
#include <stdbool.h>
#include <signal.h>

//
// Displays digital time on TM1637
// Writen by Mishin Alexander, 2018
//

// For libgpio it is GPIO numbers not PIN's
// e.g. for RPI2 GPIO 20 is PIN 38, GPIO 21 is PIN 40
#define CLK 20
#define DIO 21

int8_t TimeDisp[] = {0x00, 0x00, 0x00, 0x00};

// Create connection i set brightness
TM1637 disp(CLK, DIO); // clock, data pins

static void
termination_handler (int signum)
{
  // Off the display and terminate connection
  disp.displayOff();

  // Restore original signals behavior
  signal(SIGINT, SIG_DFL);
  signal(SIGTERM, SIG_DFL);
  // ...and terminate itself
  kill(getpid(), signum);
}

int
main(int argc, char **argv)
{
  time_t rawtime;
  struct tm tm, *timeinfo;
  struct timespec rqtp = { 0, 500000000L }; //Half a second
  bool clockPoint = true;

  disp.set(BRIGHT_DARKEST); // Set a brightness (my tm1637 is too bright for me)
  disp.displayOn();

  // Intercept signals to our function
  if (signal (SIGINT, termination_handler) == SIG_IGN)
    signal (SIGINT, SIG_IGN);
  if (signal (SIGTERM, termination_handler) == SIG_IGN)
    signal (SIGTERM, SIG_IGN);

  // Wait for half a second
  while(nanosleep(&rqtp, &rqtp) == 0) {
    int8_t loMin;

    // Get local time (with timezone)
    time (&rawtime);
    timeinfo = localtime_r(&rawtime, &tm);

    // Two times per second toggle a clock point
    clockPoint = not(clockPoint);
    disp.point(clockPoint);

    // Prepare new display digits array if a time change occurs or use old one
    loMin = timeinfo->tm_min % 10;
    if (loMin != TimeDisp[3]) {
      TimeDisp[0] = timeinfo->tm_hour / 10;
      TimeDisp[1] = timeinfo->tm_hour % 10;
      TimeDisp[2] = timeinfo->tm_min / 10;
      TimeDisp[3] = loMin; // Calculated already

      // Display a just prepared time array
      disp.display(TimeDisp);
    }
    else {
      // Display a clock point only with a second digit rewrite,
      // since a clock point is its segment.
      // The last value of second digit is ok as the time has not changed yet
      disp.display(1, TimeDisp[1]);
    }

  }

  return 0;
}

#include <tm1637.h>
#include <time.h>

//
// Displays a custom string on TM1637
// Writen by Mishin Alexander, 2018
//

// For libgpio it is GPIO numbers not PIN's
// e.g. for RPI2 GPIO 20 is PIN 38, GPIO 21 is PIN 40
#define CLK 20
#define DIO 21

int8_t TimeDisp[MAX_COLOM] = {0};

// Create connection i set brightness
TM1637 disp(CLK, DIO); // clock, data pins

int
main(int argc, char **argv)
{
  struct timespec rqtp = { 1, 0 }; //One second
  int range[2] = {10, -10};

  disp.point(POINT_OFF); // Set of a clock colon
  disp.set(BRIGHT_DARKEST); // Set a brightness (my tm1637 is too bright for me)
  disp.displayOn();

// забиваем массив буквами из библиотеки
  int8_t hello_world[] = { _i, _t, _empty, _i, _S, _empty,
                           _1, _6, _3, _7,
                           _empty, _d, _i, _S, _P, _L, _A, _y
                          };
  // выводим на дисплей со смещением на один шаг
  uint8_t numberOfChars = sizeof(hello_world);
  unsigned int delayOfChar = 300;

  disp.runningString(hello_world, numberOfChars, delayOfChar);
  nanosleep(&rqtp, &rqtp); //Sleep for a second... urgh... or less (man nanosleep)

  // Countdown of integer with leading zeros
  int counter1 = range[0];
  while(nanosleep(&rqtp, &rqtp) == 0) {
    disp.displayIntZero(counter1);
    if(counter1-- < range[1])
    {
      disp.clearDisplay();
      break;
    }
  }

  nanosleep(&rqtp, &rqtp); //Sleep for a second once again

  // Count of integer without leading zeros
  int counter2 = range[1];
  while(nanosleep(&rqtp, &rqtp) == 0) {
    disp.displayInt(counter2);
    if(counter2++ > range[0])
    {
      disp.clearDisplay();
      break;
    }
  }

  disp.displayOff();
}
